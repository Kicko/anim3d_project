# Rapport du projet Animation 3D

Amélie Bertin

Gilab link: [https://gitlab.com/Kicko/anim3d_project](https://gitlab.com/Kicko/anim3d_project)  
Video link: [https://uqload.com/u5dtfvr3gd0t.html](https://uqload.com/u5dtfvr3gd0t.html)

### Introduction

J'ai mélangé le premier et le dernier TPs du cours d'Animation 3D pour créer
l'animation d'un personnage qui court sur un chemin, avec un mouvement fluide,
et on peut modifier les points de ce chemin.

### Comment ça marche, les grandes étapes de rendu

Grâce aux fichiers fournis, nous avons accès à une classe mère qui représente
une scène, et nous avons juste à remplir notre scène avec 2 fonctions:  
- `setup_data`, qui est appelé une seule fois au début du programme  
- `frame_draw`, appelée pour chaque frame

Ce projet utilise OpenGL pour le rendu, mais la plupart les spécificités
d'OpenGL sont gérées dans les fichiers fournis, et notamment dans
`main/main.cpp`.

### La scène

La scène est composée d'un personnage muni de son squelette et de son mesh, et
de positions-clé, de sphères pour la partie trajectoire.

J'ai laissé les données du personnage telles qu'elles étaient fournies lors du
TP, mais j'ai adapté les positions et temps de la trajectoire à mon bon vouloir.
Le chemin est maintenant de la forme d'un sapin de noël, s'étendant sur l'axe
des $z$, plutôt plat sur les $y$, excepté au niveau de la pointe de l'arbre, ce
qui nous donne un chemin avec beaucoup de virages et une montée-descente. Cette
caractéristique va nous être utile pour montrer les résultats de l'interpolation
de la trajectoire.

![path](pictures/path-2.png){width=320 height=180}

J'ai également rajouté par rapport aux TPs, le déplacement du personnage, celui
ci remplace l'ancienne sphère, et la rotation du personnage, en fonction de la
direction de la trajectoire.

### Interpolation de la trajectoire

A partir des positions-clé du chemin par lequel nous souhaitons passer, on veut
produire une animation de mouvement fluide, donc sans changement brusque de
direction. Pour cela, nous allons calculer les différentes positions entre
celles clé gràce à une interpolation spline, meilleure qu'une interpolation
polynomiale car moins de grandes oscillations entre les points.

On utilise ici, une interpolation spline cubique, avec l'algorithme suivant:

1- On cherche sur quel segment du chemin on est à un instant $t$. Il est compris
entre les instants $t_i$ et $t_{i+1}$ du segment $i$ qui a pour extrémités les
positions $p_i$ et $p_{i+1}$ (les instants sont données en même temps que les
positions à l'initialisation).

2- On calcule les tangentes d'entrée et de sortie aux extrémités de ce segment.
Pour cela, on a besoin de connaitre les instants et positions des points-clé
$i-1$ et $i+2$:
$$
d_i = \frac{p_{i+1} - p{i-1}}{t_{i+1} - t_{i-1}}
$$
$$
d_{i+1} = \frac{p_{i+2} - p{i}}{t_{i+2} - t_{i}}
$$

3- On calcule la nouvelle position:
$$
p(t) = (2s^3 - 3s^2 + 1) \times p_i + (s^3 - 2s^2 + s) \times d_i + (-2s^3 +
3s^2) \times p_{i+1} + (s^3 - s^2) \times d_{i+1}
$$
avec $s = \frac{t - t_i}{t_{i+1} - t_i}$

On va donc déplacer notre objet vers cette position, ce qui nous donne ce type
de résultat:

![trajectoire1](pictures/trajectoire1-2.png "Trajectoire frame1"){width=250 height=250}
![trajectoire2](pictures/trajectoire2-2.png "Trajectoire frame2"){width=250 height=250}

### Mouvement du personnage

#### Animation du squelette
$\newline$
Comme dit précédemment, on représente notre personnage avec son squelette et
son mesh. Pour simplifier les animations, les différents déplacements sont
produits à partir de positions-clé sur le squelette, et les positions des
différents points du mesh sont interpolées à partir des positions des os du
squelette.

Plus en détail, un os du squelette à:  
- une position et une rotation de repos (sans aucune animation)  
- une liste de positions et rotations liées à des temps pour l'animation  
- un os parent

Animer les os du squelette revient donc à interpoler, pour chaque os, sa
position et rotation, en fonction de la dernière et de la prochaine position-clé
à un instant t. On utilise l'interpolation `slerp` (ou _spherical linear
interpolation_). Il permet un mouvement constant et fluide qui suit un arc de
cercle, d'une rotation clé à une autre.  
Une caractéristique à faire attention pour cette interpolation (et qui m'a posé
des problèmes la première fois), est le paramètre $t$, il doit être compris
entre 0 et 1. Il représente le pourcentage que nous avons parcouru sur cette
rotation, on peut donc le calculer en faisant
$$
t = \frac{t_{current} - t_{anim[k]}}{t_{anim[k+1]} - t_{anim[k]}}
$$
avec $t_{anim[i]}$ l'instant $i$ de l'animation, $t_{current}$ l'instant t de
notre scène, qui est compris entre $t_{anim[k]}$ et $t_{anim[k+1]}$.

L'interpolation `slerp` se calcule de la manière suivante:
$$
q_{current} = \frac{\sin((1 - t) \times \Omega)}{\sin(\Omega)} \times q_{k} +
\frac{\sin(t \times \Omega)}{\sin(\Omega)} \times q_{k+1}
$$
avec $\cos(\Omega) = q_{k} \cdot q_{k+1}$; $q_{k}$ étant le quaternion de
la position clé $k$ de l'animation.

Il suffit ensuite d'assigner cette rotation à l'os que l'on vient d'interpoler,
puis de passer aux suivants.

![skeleton1](pictures/skeleton1-2.png "Squelette frame1"){width=250 height=200 }
![skeleton2](pictures/skeleton2-2.png "Squelette frame2"){width=250 height=200 }

#### Déformation du mesh

$\newline$
La déformation de chaque point du mesh est calculée dans l'espace global de la
scène, contrairement aux rotations des os qui étaient dans l'espace local.

Pour chaque point, on va interpoler sa position à partir des os dont il est
influencé, par certains poids. Dit autrement, la position du point sera la
somme des positions de ce même point comme s'il appartenait uniquement à l'os
étudié, qu'on multiplirait par un certain poids. Ce qui nous donne la formule:
$$
p = \sum_{k=0}^{N-1}{\alpha_{k}} p_{k}
$$
avec $p_{k}$ la position du point s'il appartenait uniquement à l'os $k$.

$p_{k}$ est calculé à partir des torseurs (ou frame) de l'os au repos et dans
la situation courante, ainsi qu'à partir de la position du point au repos, de la
façon suivante:
$$
p_{k} = T_{k} \times (T_{k}^0)^{-1} \times p_{k}^0
$$
Les torseurs ont la forme suivante:
$$
T = \begin{pmatrix} R & T \\ 0 & 1 \end{pmatrix}
$$
où $R$ est une matrice contenant les différentes rotations et $T$ est un vecteur
de translation.

![mesh1](pictures/mesh1-2.png "Mesh frame1"){width=150 height=200}
![mesh2](pictures/mesh2-2.png "Mesh frame1"){width=150 height=200}

#### Suivre la trajectoire

$\newline$
(Rajout par rapport aux TPs)

La première étape est de remplacer l'objet qui se déplaçait sur la trajectoire
par le personnage. Sauf que le personnage n'a pas un seul point mais une
multitude, j'ai donc rajouté un champs `vec3 global_pos` à la structure
représentant le mesh du personnage (celui ci étant directement utilisé dans
l'affichage). Cette variable sera mise à jour après le calcul de l'interpolation
spline expliquée précédemment, à la place de la position de l'ancienne sphère.
Ensuite on utilise cette position globale dans le calcul des points courants
du mesh avec une simple somme pour les déplacer.
$$
p = p + \text{global pos}
$$

La deuxième étape est d'orienter le personnage vers la direction qu'il emprunte.
Pour cela, il faut calculer cette direction. J'ai donc récupérer la valeur de
`global_pos` à la frame précedente et sa valeur courante, la direction étant
la différence des 2.

Ensuite on calcule l'angle correspondant à cette direction. J'étais d'abord
parti sur une formule utilisant `tan`, en représentant mon problème par un
triangle rectangle, mais je me suis aperçu que certains angles n'appartenaient
pas à $[ -\frac{\pi}{2}, \frac{\pi}{2} ]$. J'ai donc utilisé une autre formule:
$$
\cos(\theta) = \frac{d \cdot z}{\lVert d \rVert \times \lVert z \rVert}
$$
avec $z$, le vecteur direction de la position de repos, qui a pour valeur
$(0, 0, -1)$

On calcule enfin la nouvelle position du point, dont on a appliqué cette
rotation, grâce à la matrice de rotation de l'axe $y$:
$$
M = \begin{pmatrix}
        \cos(\theta) & 0 & \sin(\theta) \\
        0 & 1 & 0 \\
        -\sin(\theta) & 0 & \cos(\theta)
    \end{pmatrix}
$$

$\newline$
Ce qui nous donne, pour chaque point du mesh:
$$
p = M \times p + \text{global pos}
$$


![all1](pictures/all1-2.png "All frame1"){width=250 height=150}
![all2](pictures/all2-2.png "All frame1"){width=250 height=150}

[*Animation video link*](https://uqload.com/u5dtfvr3gd0t.html)

### Problèmes rencontrés

J'ai eu quelques soucis dans le calcul des angles, tout d'abord avec $\theta$,
je n'utilisais pas la bonne formule. Puis, je me suis mélangé les pinceaux avec
les degrés et les radians, je pensais qu'il fallait convertir mais en fait non.  
Et aussi, il m'a paru très étrange que le personnage ait une orientation
uniquement vers les $x$ positifs, ce que j'ai réglé en changeant le signe
de $theta$ si le vecteur direction a une valeur $x$ négative.

### Debrief

C'était des TPs et un projet très cool à faire, pas de grosse prise de tête sur
OpenGL grâce à la lib fournie (thanks), donc on pouvait se concentrer sur les
math pour créer nos animations.
