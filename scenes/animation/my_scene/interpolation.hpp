#pragma once

#ifdef MY_SCENE


/** Function returning the index i such that t \in [v[i].t,v[i+1].t] */
size_t index_at_value(float t, const vcl::buffer<vec3t> &v);

vcl::vec3 linear_interpolation(float t, float t1, float t2, const vcl::vec3& p1, const vcl::vec3& p2);

vcl::vec3 cardinal_spline_interpolation(float t, float t0, float t1, float t2,
    float t3, const vcl::vec3& p0, const vcl::vec3& p1, const vcl::vec3& p2,
    const vcl::vec3& p3);

#endif
