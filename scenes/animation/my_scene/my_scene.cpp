#include "my_scene.hpp"
#ifdef MY_SCENE

#include "interpolation.hpp"
#include "skinning_loader.hpp"

#define PI 3.14159265358979323846f

using namespace vcl;


buffer<joint_geometry> interpolate_skeleton_at_time(
    float time, const buffer<buffer<joint_geometry_time>>& animation)
{
    // Compute skeleton corresponding to a given time from key poses
    // - animation[k] corresponds to the kth animated joint (vector of joint
    // geometry at given time)
    // - animation[k][i] corresponds to the ith pose of the kth joint
    //      - animation[k][i].time         -> corresponding time
    //      - animation[k][i].geometry.p/r -> corresponding position, rotation

    size_t N_joint = animation.size();
    buffer<joint_geometry> skeleton;
    skeleton.resize(N_joint);

    for (size_t k_joint = 0; k_joint < N_joint; ++k_joint)
    {
        const buffer<joint_geometry_time>& joint_anim = animation[k_joint];

        // Find the index corresponding to the current time
        size_t k_current = 0;
        assert_vcl_no_msg(joint_anim.size() > k_current + 1);
        while (time > joint_anim[k_current + 1].time)
        {
            ++k_current;
            assert_vcl_no_msg(joint_anim.size() > k_current + 1);
        }

        // Compute correct interpolation of joint geometry
        // (the following code corresponds to nearest neighbors, not to a smooth
        // interpolation)
        joint_geometry current_geometry = joint_anim[k_current].geometry;

        size_t size = joint_anim.size();
        joint_geometry next_geometry =
            joint_anim[(k_current + 1) % size].geometry;

        next_geometry.r = slerp(
            current_geometry.r, next_geometry.r,
            (time - joint_anim[k_current].time)
            / (joint_anim[k_current + 1].time - joint_anim[k_current].time)
        );

        skeleton[k_joint] = next_geometry;
    }

    return skeleton;
}

void compute_skinning(skinning_structure& skinning,
                      const buffer<joint_geometry>& skeleton_current,
                      const buffer<joint_geometry>& skeleton_rest_pose,
                      const vec3& prev_pos)
{
    const size_t N_vertex = skinning.rest_pose.size();

    // Compute Rotation of the character in the scene
    float bx = skinning.global_pos.x - prev_pos.x;
    float bz = skinning.global_pos.z - prev_pos.z;
    float theta = std::acos(
        bz
        / std::sqrt(bx * bx + bz * bz)
    ) + PI;
    if (bx < 0)
    {
        theta = -theta;
    }
    vcl::mat3 scene_rotation(
         std::cos(theta), 0, std::sin(theta),
         0,               1, 0,
        -std::sin(theta), 0, std::cos(theta)
    );


    for (size_t k = 0; k < N_vertex; ++k)
    {
        // Compute skinning deformation
        vcl::mat4 res_matrix;
        for (skinning_influence influence : skinning.influence[k])
        {
            size_t joint = influence.joint;
            joint_geometry geometry = skeleton_current[joint];
            vcl::mat3 rotation_matrix = geometry.r.matrix();
            vcl::mat4 frame =
                vcl::mat4::from_mat3_vec3(rotation_matrix, geometry.p);

            joint_geometry geometry2 = skeleton_rest_pose[joint];
            vcl::mat3 rotation_matrix2 = geometry2.r.matrix();
            // inverse of rotation = transpose
            vcl::mat4 frame2 = vcl::mat4::from_mat3_vec3(
                transpose(rotation_matrix2),
                transpose(rotation_matrix2) * -geometry2.p);

            vcl::mat4 matrix = frame * frame2;

            res_matrix += influence.weight * matrix;
        }

        vec4 pres_pos = vec4(skinning.rest_pose[k].x, skinning.rest_pose[k].y,
                             skinning.rest_pose[k].z, 1);
        vec4 new_pos = res_matrix * pres_pos;
        skinning.deformed.position[k] = vec3(new_pos.x, new_pos.y, new_pos.z);

        // Apply Rotation and Translation to position of the mesh in the scene
        skinning.deformed.position[k] = scene_rotation
            * skinning.deformed.position[k]
            + skinning.global_pos;
    }
}

// Convert skeleton from local to global coordinates
buffer<joint_geometry>
local_to_global(const buffer<joint_geometry>& local,
                const buffer<joint_connectivity>& connectivity)
{
    const size_t N = connectivity.size();
    assert(local.size() == connectivity.size());
    std::vector<joint_geometry> global;
    global.resize(N);
    global[0] = local[0];

    // T_global = T_global^parent * T_local (T: 4x4 transformation matrix)
    //   => R_global = R_global^parent * R_local
    //   => P_global = R_global^parent * P_local + P_global^parent
    for (size_t k = 1; k < N; ++k)
    {
        const int parent = connectivity[k].parent;
        global[k].r = global[parent].r * local[k].r;
        global[k].p = global[parent].r.apply(local[k].p) + global[parent].p;
    }

    return global;
}

void scene_model::setup_data(std::map<std::string, GLuint>& shaders,
                             scene_structure& scene, gui_structure&)
{
    shader_mesh = shaders["mesh"];

    glEnable(GL_POLYGON_OFFSET_FILL);

    // Init gui parameters
    gui_param.display_mesh = true;
    gui_param.display_wireframe = false;
    gui_param.display_rest_pose = false;
    gui_param.display_texture = true;
    gui_param.display_type = display_character;

    // Load initial cylinder model
    load_character_data(skeleton, skinning, character_visual, timer_perso,
                        shader_mesh);

    // Set camera orientation
    scene.camera.apply_rotation(-0.15, 0.45, 0, 0);
    scene.camera.apply_scaling(3.2);


    /*         PATH             */
    // Initial Keyframe data vector of (position, time)
    keyframes = { { { 2.5,   0,   0}, 0.0f  },
                  { { 1.2,   0,-1.5}, 0.7f  },
                  { { 2.2,   0,  -2}, 1.4f  },
                  { { 0.8,   0,-3.2}, 2.0f  },
                  { { 1.3,   0,-4.2}, 2.5f  },
                  { { 0.4, 0.2,-4.5}, 3.0f  },
                  { {   0, 0.5,-5.1}, 3.5f  },
                  { {-0.4, 0.2,-4.5}, 4.0f  },
                  { {-1.3,   0,-4.2}, 4.5f  },
                  { {-0.8,   0,-3.2}, 5.0f  },
                  { {-2.2,   0,  -2}, 6.0f  },
                  { {-1.2,   0,-1.5}, 7.0f  },
                  { {-2.5,   0,   0}, 8.0f  },
                  { { 2.5,   0,   0}, 9.5f  },
                };

    // Set timer bounds
    // You should adapt these extremal values to the type of interpolation
    timer_path.t_min = keyframes[0].t;                   // first time of the keyframe
    timer_path.t_max = keyframes[keyframes.size()-1].t;  // last time of the keyframe
    timer_path.t = timer_path.t_min;

    // Prepare the visual elements
    keyframe_visual = mesh_primitive_sphere();
    keyframe_visual.shader = shaders["mesh"];
    keyframe_visual.uniform.color = {1,1,1};
    keyframe_visual.uniform.transform.scaling = 0.05f;

    keyframe_picked = mesh_primitive_sphere();
    keyframe_picked.shader = shaders["mesh"];
    keyframe_picked.uniform.color = {1,0,0};
    keyframe_picked.uniform.transform.scaling = 0.055f;

    segment_drawer.init();

    trajectory = curve_dynamic_drawable(100); // number of steps stored in the trajectory
    trajectory.uniform.color = {0,0,1};

    picked_object=-1;
}

void scene_model::frame_draw(std::map<std::string, GLuint>& shaders,
                             scene_structure& scene, gui_structure&)
{
    timer_perso.update();
    timer_path.update();

    set_gui();

    /*         PATH             */
    const float t = timer_path.t;
    if( t<timer_path.t_min+0.1f ) // clear trajectory when the timer restart
        trajectory.clear();

    // ********************************************* //
    // Compute interpolated position at time t
    // ********************************************* //
    const size_t idx = index_at_value(t, keyframes);

    // Assume a closed curve trajectory
    const size_t N = keyframes.size();

    // Preparation of data for the linear interpolation
    // Parameters used to compute the linear interpolation
    const float t1 = keyframes[idx  ].t; // = t_i
    const float t2 = keyframes[idx+1].t; // = t_{i+1}

    const vec3& p1 = keyframes[idx  ].p; // = p_i
    const vec3& p2 = keyframes[idx+1].p; // = p_{i+1}

    vec3 p;
    if (idx < 1 || idx >= N - 2)
    {
        // Compute the linear interpolation here
        p = linear_interpolation(t,t1,t2,p1,p2);
    }
    else
    {
        const float t0 = keyframes[idx-1].t; // = t_{i-1}
        const float t3 = keyframes[idx+2].t; // = t_{i+2}
        const vec3& p0 = keyframes[idx-1].p; // = p_{i-1}
        const vec3& p3 = keyframes[idx+2].p; // = p_{i+2}

        p = cardinal_spline_interpolation(t, t0, t1, t2, t3, p0, p1, p2, p3);
    }

    // Store current trajectory of point p
    trajectory.add_point(p);

    const vec3 prev_pos = skinning.global_pos;

    // Draw current position
    skinning.global_pos = p;

    // Draw moving point trajectory
    trajectory.draw(shaders["curve"], scene.camera);


    // Draw sphere at each keyframe position
    if(gui_scene.display_keyframe) {
        for(size_t k=0; k<N; ++k)
        {
            const vec3& p_keyframe = keyframes[k].p;
            keyframe_visual.uniform.transform.translation = p_keyframe;
            draw(keyframe_visual, scene.camera);
        }
    }

    // Draw selected sphere in red
    if( picked_object!=-1 )
    {
        const vec3& p_keyframe = keyframes[picked_object].p;
        keyframe_picked.uniform.transform.translation = p_keyframe;
        draw(keyframe_picked, scene.camera);
    }


    // Draw segments between each keyframe
    if(gui_scene.display_polygon) {
        for(size_t k=0; k<keyframes.size()-1; ++k)
        {
            const vec3& pa = keyframes[k].p;
            const vec3& pb = keyframes[k+1].p;

            segment_drawer.uniform_parameter.p1 = pa;
            segment_drawer.uniform_parameter.p2 = pb;
            segment_drawer.draw(shaders["segment_im"], scene.camera);
        }
    }

    /*      PERSONNAGE      */
    const float t_perso = timer_perso.t;

    const auto skeleton_geometry_local =
        interpolate_skeleton_at_time(t_perso, skeleton.anim);
    const auto skeleton_rest_pose =
        local_to_global(skeleton.rest_pose, skeleton.connectivity);
    auto skeleton_current =
        local_to_global(skeleton_geometry_local, skeleton.connectivity);

    if (gui_param.display_rest_pose)
        skeleton_current = skeleton_rest_pose;

    compute_skinning(skinning, skeleton_current, skeleton_rest_pose, prev_pos);
    character_visual.update_position(skinning.deformed.position);

    character_visual.update_normal(skinning.deformed.normal);

    if (gui_param.display_mesh)
    {
        glPolygonOffset(1.0, 1.0);
        GLuint const texture_id =
            (gui_param.display_texture ? character_visual.texture_id
                                       : scene.texture_white);
        draw(character_visual, scene.camera, character_visual.shader,
             texture_id);
    }
    if (gui_param.display_wireframe)
    {
        glPolygonOffset(1.0, 1.0);
        draw(character_visual, scene.camera, shaders["wireframe_quads"]);
    }


}


void scene_model::set_gui()
{
    ImGui::SliderFloat("Timer run anim", &timer_perso.t, timer_perso.t_min, timer_perso.t_max, "%.2f s");
    ImGui::SliderFloat("Time run anim scale", &timer_perso.scale, 0.05f, 2.0f, "%.2f s");
    ImGui::SliderFloat("Timer", &timer_path.t, timer_path.t_min, timer_path.t_max, "%.2f s");
    ImGui::SliderFloat("Time scale", &timer_path.scale, 0.05f, 2.0f, "%.2f s");

    /*         PATH         */
    ImGui::Text("Display: "); ImGui::SameLine();
    ImGui::Checkbox("keyframe", &gui_scene.display_keyframe); ImGui::SameLine();
    ImGui::Checkbox("polygon", &gui_scene.display_polygon);

    /*      PERSONNAGE      */
    ImGui::Text("Display Mesh:");
    ImGui::Checkbox("Mesh", &gui_param.display_mesh);
    ImGui::SameLine();
    ImGui::Checkbox("Wireframe", &gui_param.display_wireframe);
    ImGui::SameLine();
    ImGui::Checkbox("Texture", &gui_param.display_texture);

    ImGui::Checkbox("Rest pose", &gui_param.display_rest_pose);

    // Start and stop animation
    bool const stop = ImGui::Button("Stop");
    ImGui::SameLine();
    bool const start = ImGui::Button("Start");
    ImGui::SameLine();

    if (stop)
    {
        timer_perso.stop();
        timer_path.stop();
    }
    if (start)
    {
        timer_perso.start();
        timer_path.start();
    }
}

#endif
