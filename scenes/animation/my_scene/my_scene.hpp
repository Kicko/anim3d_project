#pragma once

#include "main/scene_base/base.hpp"
#ifdef MY_SCENE

#include "quaternion.hpp"

/*      PERSONNAGE PART         */
// Connectivity information of a joint in the hierarchy
//  Store parent index, and the current name
struct joint_connectivity
{
    int parent;
    std::string name;
};

// 3D Geometry of a joint (position p, and rotation r)
struct joint_geometry
{
    vcl::vec3 p;
    quaternion r;
};

// Key pose of a joint (Key-time, and geometry at this time)
struct joint_geometry_time
{
    float time;
    joint_geometry geometry;
};

// Storage of the influence of a joint for a given vertex
struct skinning_influence
{
    int joint; // index of the corresponding joint
    float weight; // skinning weight of this joint
};

// Structure storing skeleton data to perform skinning afterward
struct skeleton_structure
{
    // Connectivity of the skeleton
    vcl::buffer<joint_connectivity> connectivity;
    // Skeleton of the rest pose expressed in local coordinates
    vcl::buffer<joint_geometry> rest_pose;
    // Skeleton animation expressed in local coordinates (N_joint x N_time)
    vcl::buffer<vcl::buffer<joint_geometry_time>> anim;
};

// Storage structure to perform skinning deformation of a surface
struct skinning_structure
{
    // Skinning weights: for each vertex, store all influence values
    // (bone+weight)
    vcl::buffer<vcl::buffer<skinning_influence>> influence;
    // 3D position of the mesh in rest pose
    vcl::buffer<vcl::vec3> rest_pose;
    // 3D normals of the mesh in rest pose
    vcl::buffer<vcl::vec3> rest_pose_normal;
    // Deformed mesh
    vcl::mesh deformed;
    // Position of the character in scene
    vcl::vec3 global_pos;
};

enum gui_parameters_display_type
{
    display_character,
};
struct gui_parameters
{
    bool display_mesh;
    bool display_rest_pose;
    bool display_wireframe;
    bool display_texture;
    int display_type;
};


/*      INTERPOLATION PATH PART         */
// Store a vec3 (p) + time (t)
struct vec3t{
    vcl::vec3 p; // position
    float t;     // time
};

struct gui_scene_structure{
    bool display_keyframe = true;
    bool display_polygon  = true;
};


/*      SCENE       */
struct scene_model : scene_base
{
    void setup_data(std::map<std::string, GLuint>& shaders,
                    scene_structure& scene, gui_structure& gui);
    void frame_draw(std::map<std::string, GLuint>& shaders,
                    scene_structure& scene, gui_structure& gui);
    void set_gui();

    vcl::timer_interval timer_perso;
    vcl::timer_interval timer_path;


    // personnage
    skeleton_structure skeleton;
    skinning_structure skinning;
    vcl::mesh_drawable character_visual;
    GLuint shader_mesh;

    gui_parameters gui_param;


    // path
    void mouse_click(scene_structure& scene, GLFWwindow* window, int button, int action, int mods);
    void mouse_move(scene_structure& scene, GLFWwindow* window);

    vcl::buffer<vec3t> keyframes; // Given (position,time)

    vcl::mesh_drawable keyframe_visual;
    vcl::mesh_drawable keyframe_picked;
    vcl::segment_drawable_immediate_mode segment_drawer;
    vcl::curve_dynamic_drawable trajectory;

    // Store the index of a selected sphere
    int picked_object;

    gui_scene_structure gui_scene;
};

#endif
