#include "skinning_loader.hpp"
#ifdef MY_SCENE

#include <fstream>
#include <sstream>

using namespace vcl;

void load_character_data(skeleton_structure& skeleton,
                         skinning_structure& skinning,
                         mesh_drawable& shape_visual, timer_interval& timer,
                         GLuint shader)
{
    timer = timer_interval();

    const float scaling = 0.005f;

    // Load mesh
    buffer<buffer<int>> vertex_correspondance;
    mesh character = mesh_load_file_obj(
        "scenes/animation/my_scene/assets/marine/mesh.obj",
        vertex_correspondance);

    skeleton.connectivity = read_skeleton_connectivity(
        "scenes/animation/my_scene/assets/marine/skeleton_connectivity");
    skeleton.rest_pose = read_skeleton_geometry(
        "scenes/animation/my_scene/assets/marine/skeleton_geometry_local",
        scaling);
    buffer<buffer<skinning_influence>> influence = read_skinning_influence(
        "scenes/animation/my_scene/assets/marine/skinning_data");
    skinning.influence = map_correspondance(influence, vertex_correspondance);

    // 3 possibles animations:
    // skeleton.anim = read_skeleton_animation(
    //     "scenes/animation/my_scene/assets/marine/skeleton_animation_idle",
    //     scaling
    // );
    // timer.t_max = 4.0f
    // skeleton.anim = read_skeleton_animation(
    //     "scenes/animation/my_scene/assets/marine/skeleton_animation_walk",
    //     scaling
    // );
    // timer.t_max = 1.0f;
    skeleton.anim = read_skeleton_animation(
        "scenes/animation/my_scene/assets/marine/skeleton_animation_run",
        scaling
    );
    timer.t_max = 0.733f;

    GLuint texture_id = create_texture_gpu(image_load_png(
        "scenes/animation/my_scene/assets/marine/texture.png"));

    for (vec3& p : character.position)
        p *= scaling; // scale vertices of the mesh
    shape_visual.clear();
    shape_visual = mesh_drawable(character);
    shape_visual.shader = shader;
    shape_visual.uniform.shading.specular = 0.1f;
    shape_visual.uniform.shading.specular_exponent = 8;

    character.fill_empty_fields();
    skinning.rest_pose = character.position;
    skinning.rest_pose_normal = character.normal;
    skinning.deformed = character;

    shape_visual.texture_id = texture_id;
}

buffer<buffer<skinning_influence>>
read_skinning_influence(const std::string& filename)
{
    buffer<buffer<skinning_influence>> influence;

    std::ifstream fid(filename);

    // first line = number of influence per pertex (fixed for all vertices)
    size_t N_bone_influence = 0;
    fid >> N_bone_influence;

    assert_vcl(fid.good(), "Cannot read file " + filename);
    assert_vcl_no_msg(N_bone_influence > 0 && N_bone_influence <= 6);

    // Read influence associated to each vertex
    std::vector<skinning_influence> skinning_vertex;
    skinning_vertex.resize(N_bone_influence);

    while (fid.good())
    {
        // read list of [bone index] [skinning weights]
        for (size_t k = 0; k < N_bone_influence && fid.good(); ++k)
            fid >> skinning_vertex[k].joint >> skinning_vertex[k].weight;

        if (fid.good())
            influence.push_back(skinning_vertex);
    }

    fid.close();

    // normalize skinning weights (sum up to one)
    for (size_t kv = 0; kv < influence.size(); ++kv)
    {
        float w = 0.0f;
        // compute weight sum
        for (size_t k = 0; k < N_bone_influence; ++k)
            w += influence[kv][k].weight;
        // normalize
        if (w > 1e-5f)
            for (size_t k = 0; k < N_bone_influence; ++k)
                influence[kv][k].weight /= w;
    }

    return influence;
}

buffer<buffer<joint_geometry_time>>
read_skeleton_animation(const std::string& filename, float scaling)
{
    buffer<buffer<joint_geometry_time>> skeleton;

    std::ifstream fid(filename);
    while (fid.good())
    {
        size_t N_key = 0;
        fid >> N_key;

        if (fid.good())
        {
            buffer<joint_geometry_time> animated_joint;
            animated_joint.resize(N_key);

            for (size_t k_key = 0; k_key < N_key; ++k_key)
            {
                float key_time;
                vec3 p;
                vec4 q;

                fid >> key_time;
                fid >> p.x >> p.y >> p.z;
                fid >> q.x >> q.y >> q.z >> q.w;

                q = normalize(q);

                animated_joint[k_key] = {key_time, {p * scaling, q}};
            }

            skeleton.push_back(animated_joint);
        }
    }

    fid.close();

    return skeleton;
}

buffer<joint_connectivity>
read_skeleton_connectivity(const std::string& filename)
{
    buffer<joint_connectivity> skeleton;

    std::ifstream fid(filename);

    while (fid.good())
    {
        std::string line;
        std::getline(fid, line);

        if (fid.good())
        {
            std::stringstream sstream(line);

            int k;
            int parent;
            std::string name;

            sstream >> k >> parent >> name;

            skeleton.push_back({parent, name});
        }
    }

    fid.close();

    return skeleton;
}

buffer<joint_geometry> read_skeleton_geometry(const std::string& filename,
                                              float scaling)
{
    buffer<joint_geometry> skeleton;

    std::ifstream fid(filename);
    while (fid.good())
    {
        std::string line;
        std::getline(fid, line);

        if (fid.good())
        {
            std::stringstream sstream(line);

            vec3 p;
            quaternion q;

            sstream >> p.x >> p.y >> p.z;
            sstream >> q.x >> q.y >> q.z >> q.w;

            // q = normalize(q);

            skeleton.push_back({p * scaling, q});
        }
    }

    fid.close();

    return skeleton;
}

#endif
